# Helper

## Changelog

- add : Helper::arrayKeysToTag($tb, '/');

## Import

```bash
composer require wyzen-packages/helper
```

## usage

```php
use Wyzen\Php\Helper;
```

## Liste des méthodes

- **HelperException**
- **arrayPagination**(&data: array, [page: int = 1], [limit: int = 20]): stdClass
- **camelCase**(str: string, [firstChar: string = 'lcfirst']): string
- **classBasename**(class: object|string): string
- **compareValues**([arr1: array|mixed = [...]], [arr2: array|mixed = [...]], [operateur: string = 'IN']): bool
- **createDatetimeFromString**(strDate: null|string, [format: null|string = null]): DateTime|null
- **findInArrayByKeys**(&data: array, ...keys: array): array|mixed|null
- **findInArrayByTag**(&data: array, tag: string): array|mixed|null
- **getBlacklistExtensionFile**(): array|string[]
- **getValueEx**(a: array|null, key: string, [defaultValueOrException: mixed = null], [classMethodValidator: array|null = null]): mixed|null
- **getValueFromTag**(tag: string, &data: array): array|mixed|null
- **indexedByKey**(&array: array, reorderByKeyname: string, [append: bool = false]): array
- **parseArray**(val, [strict: bool = false]): array|null
- **parseBool**(val: bool|null|string, [strict: bool = false]): bool|null
- **isDateWithValidFormat**(date: string): bool
- **parseFloat**(val: float|null|string, [strict: bool = false]): float|null
- **parseInt**(val: int|null|string, [strict: bool = false]): int|null
- **isoToUtf8**([str: string = '']): string
- **normalizeTitleDocument**(str: null|string): string
- **passwordGenerate**([min_length: int = 8], [max_length: int = 8], [options: array = [...]]): string
- **randomStr**(length: int, [keyspace: string = '0123456789abcdefghi...]): string
- **removeNullValues**([&tb: array = [...]], [recursive: bool = false]): array
- **setBlacklistExtensionFile**(ext: array): array
- **slugify**(text: string, [divider: string = '_']): string
- **utf8ToIso**([str: string = '']): string
- **arrayKeysToTag**(array, separator, prefix): array;
