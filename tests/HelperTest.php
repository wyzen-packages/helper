<?php

use PHPUnit\Framework\TestCase;
use Wyzen\Php\Helper;

class HelperTest extends TestCase
{
    private $aAssoc = [
        'a' => [
            'id' => 'id2',
            'name' => 'name 2',
        ],
        'b' => [
            'id' => 'id4',
            'name' => 'name 4',
        ],
        'c' => [
            'id' => 'id3',
            'name' => 'name 3',
        ],
        'd' => [
            'id' => 'id1',
            'name' => 'name 1',
        ],
    ];

    private $data = [
        'a' => [
            'id' => 'id2',
            'name' => 'name 2',
        ],
        'b' => [
            'id' => 'id4',
            'name' => 'name 4',
        ],
        'c' => [
            'id' => 'id3',
            'name' => 'name 3',
        ],
        'd' => [
            'id' => 'id1',
            'name' => 'name 1',
        ],
        'data' => [
            'level1' => [
                'level2' => [
                    [
                        "id" => 0,
                        "name" => "name 0",
                    ],
                    [
                        "id" => 1,
                        "name" => "name 1",
                    ],
                ],
            ]
        ]
    ];

    public function testConstruct()
    {
        $this->assertInstanceOf(
            Helper::class,
            new Helper()
        );
    }

    public function testBlacklistExtension()
    {
        $ext = ['exe', 'bin'];
        $result = Helper::setBlacklistExtensionFile($ext);
        $this->assertEquals($ext, $result);

        $resultGetter = Helper::getBlacklistExtensionFile();
        $this->assertEquals($ext, $resultGetter);
    }

    /**
     * @throws \Exception
     */
    public function testGetValueEx()
    {
        $ext = [
            'exe' => 'exe ext',
            'bin' => 'bin ext',
            'bat' => 'bat ext',
            'sh' => 'sh ext',
        ];
        $result = Helper::getValueEx($ext, 'bin');
        $this->assertEquals($ext['bin'], $result);

        $result = Helper::getValueEx($ext, 'xxx');
        $this->assertNull($result);

        $result = Helper::getValueEx($ext, 'xxx', 'default value');
        $this->assertEquals('default value', $result);

        $this->expectException(InvalidArgumentException::class);
        $result = Helper::getValueEx($ext, 'xxx', InvalidArgumentException::class);
    }

    public function testDateWithValidFormat()
    {
        $result = Helper::isDateWithValidFormat('2020-12-24 01:02:03');
        $this->assertTrue($result);


        $result = Helper::isDateWithValidFormat('2020-12-24');
        $this->assertFalse($result);
    }

    public function testCamelCase()
    {
        $str = "   hello      woRld ";
        $strLcFirst = "helloWorld";
        $strUcFirst = "HelloWorld";

        $result = Helper::camelCase($str);
        $this->assertEquals($strLcFirst, $result);

        $result = Helper::camelCase($str, 'ucfirst');
        $this->assertEquals($strUcFirst, $result);
    }

    public function testCompareValues()
    {
        $a1 = ['a', 'b', 'c', 'd', 'e'];
        $a2 = ['e', 'c', 'b', 'd', 'a'];

        $result = Helper::compareValues('b', $a1);
        $this->assertTrue($result);

        $result = Helper::compareValues(['b', 'd'], $a1);
        $this->assertTrue($result);

        $result = Helper::compareValues(['z'], $a1, 'NOTIN');
        $this->assertTrue($result);

        $result = Helper::compareValues(['a'], $a1, 'NOTIN');
        $this->assertFalse($result);

        $result = Helper::compareValues($a1, $a1, 'EQ');
        $this->assertTrue($result);

        $result = Helper::compareValues($a1, $a2, 'EQ');
        $this->assertTrue($result);
    }

    public function testRandomStr()
    {
        $result = Helper::randomStr(8);
        $this->assertEquals(8, \strlen($result));

        $result = Helper::randomStr(3);
        $this->assertEquals(3, \strlen($result));

        $result = Helper::randomStr(3, '0000000');
        $this->assertEquals('000', $result);
    }

    public function testPasswordGenerate()
    {
        $result = Helper::passwordGenerate();
        $this->assertEquals(8, \strlen($result));

        $result = Helper::passwordGenerate(10, 10);
        $this->assertEquals(10, \strlen($result));

        $result = Helper::passwordGenerate(8, 12);
        $this->assertGreaterThanOrEqual(8, \strlen($result));
        $this->assertLessThanOrEqual(12, \strlen($result));
    }

    public function testNormalizeTitleDocument()
    {
        $filename = "my filename with space.pdf";
        $result = Helper::normalizeTitleDocument($filename);
        $this->assertEquals('my_filename_with_space.pdf', $result);
    }

    public function testIndexedByKey()
    {
        $aAssoc = $this->aAssoc;
        $aAssocOrdered = [
            'id2' => [
                'id' => 'id2',
                'name' => 'name 2',
            ],
            'id4' => [
                'id' => 'id4',
                'name' => 'name 4',
            ],
            'id3' => [
                'id' => 'id3',
                'name' => 'name 3',
            ],
            'id1' => [
                'id' => 'id1',
                'name' => 'name 1',
            ]
        ];

        $result = Helper::indexedByKey($aAssoc, 'id');
        $this->assertEquals($aAssocOrdered, $result);
    }

    public function testGetValueFromTag()
    {
        $tag = 'b.name';
        $result = Helper::getValueFromTag($tag, $this->aAssoc);
        $this->assertEquals('name 4', $result);

        $tag = 'b.noname';
        $result = Helper::getValueFromTag($tag, $this->aAssoc);
        $this->assertNull($result);
    }

    public function testfindInArrayByTag()
    {
        $tag = 'b.name';
        $result = Helper::findInArrayByTag($this->data, $tag);
        $this->assertEquals('name 4', $result);

        $tag = 'b.noname';
        $result = Helper::findInArrayByTag($this->data, $tag);
        $this->assertNull($result);

        $tag = 'data.level1.level2';
        $result = Helper::findInArrayByTag($this->data, $tag);
        $this->assertEquals($this->data['data']['level1']['level2'], $result);

        $tag = 'data.level1.level2.1';
        $result = Helper::findInArrayByTag($this->data, $tag);
        $this->assertEquals($this->data['data']['level1']['level2'][1], $result);
    }

    public function testClassBasename()
    {
        $class = Helper::class;
        $base_class_name = Helper::classBasename($class);
        $this->assertEquals("Helper", $base_class_name);

        $obj = new Helper();
        $base_class_name = Helper::classBasename($obj);
        $this->assertEquals("Helper", $base_class_name);

        $base_class_name = Helper::classBasename('App\Classes\Users');
        $this->assertEquals("Users", $base_class_name);

        $base_class_name = Helper::classBasename('\Users');
        $this->assertEquals("Users", $base_class_name);

        $base_class_name = Helper::classBasename('Users');
        $this->assertEquals("Users", $base_class_name);
    }

    public function testFindInArrayByKeys()
    {
        $data = [
            "url" => "https://www.google.com",
            "options" => [
                "method" => "POST",
                "header" => [
                    'content-type' => 'application/json',
                ]
            ],
            "data" => [
                [
                    "name" => "name 0",
                    "description" => "description 0",
                ],
                [
                    "name" => "name 1",
                    "description" => "description 1",
                ],
                [
                    "name" => "name 2",
                    "description" => "description 2",
                ],
                "data_level1" => true,
                "data_level2" => null,
            ]
        ];

        $result = Helper::findInArrayByKeys($data);
        $this->assertEquals($data, $result);

        $result = Helper::findInArrayByKeys($data, 'url');
        $this->assertEquals($data['url'], $result);

        $result = Helper::findInArrayByKeys($data, 'options');
        $this->assertEquals($data['options'], $result);

        $result = Helper::findInArrayByKeys($data, 'options', 'method');
        $this->assertEquals($data['options']['method'], $result);

        $result = Helper::findInArrayByKeys($data, 'options', 'unknown');
        $this->assertNull($result);

        $result = Helper::findInArrayByKeys($data, 'data');
        $this->assertEquals($data['data'], $result);

        $result = Helper::findInArrayByKeys($data, 'data', "2");
        $this->assertEquals($data['data']['2'], $result);

        $result = Helper::findInArrayByKeys($data, 'data', "data_level1");
        $this->assertEquals($data['data']['data_level1'], $result);

        $result = Helper::findInArrayByKeys($data, 'data', "data_level2");
        $this->assertNull($result);

        // \var_dump($unknown);
    }

    /**
     * @testdox Supprime les champs de valeur NULL d'un tableau
     *
     * @return void
     */
    public function testRemoveNullValuesFromArray()
    {
        $tb = [
            'key1' => [
                'field1' => 1,
                'field2' => null,
                'field3' => 2,
            ],
            'key2' => [
                'field1' => false,
                'field2' => null,
                'field3' => 4,
            ],
            'key3' => [
                'field1' => 4,
                'field2' => true,
                'field3' => 6,
            ],
            'key4' => [
                'field1' => null,
                'field2' => null,
                'field3' => null,
            ],
        ];

        $expected = [
            'key1' => [
                'field1' => 1,
                'field3' => 2,
            ],
            'key2' => [
                'field1' => false,
                'field3' => 4,
            ],
            'key3' => [
                'field1' => 4,
                'field2' => true,
                'field3' => 6,
            ],
            'key4' => [],
        ];

        $res = Helper::removeNullValues($tb);

        $this->assertEquals($expected, $res);
    }

    /**
     * @testdox Supprime les champs de valeur NULL d'un tableau avec clé=>valeur
     *
     * @return void
     */
    public function testRemoveNullValuesFromSimpleKeyValArray()
    {
        $tb = [
            'field1' => 1,
            'field2' => null,
            'field3' => false,
            'field4' => true,
            'field5' => null,
        ];

        $expected = [
            'field1' => 1,
            'field3' => false,
            'field4' => true,
        ];

        $res = Helper::removeNullValues($tb);

        $this->assertEquals($expected, $res);
    }

    /**
     * @testdox Supprime les champs de valeur NULL d'un tableau en profondeur
     *
     * @return void
     */
    public function testRemoveNullValuesRecursiveFromArray()
    {
        $tb = [
            'key1' => [
                'field1' => 1,
                'field2' => null,
                'field3' => 2,
                'key2' => [
                    'field1' => false,
                    'field2' => null,
                    'field3' => 4,
                    'key3' => [
                        'field1' => 4,
                        'field2' => true,
                        'field3' => 6,
                        'key5' => [
                            'field1' => null,
                            'field2' => null,
                            'field3' => null,
                        ],
                    ],
                ],
            ],
            'key4' => [
                'field1' => null,
                'field2' => null,
                'field3' => null,
            ],
        ];

        $expected = [
            'key1' => [
                'field1' => 1,
                'field3' => 2,
                'key2' => [
                    'field1' => false,
                    'field3' => 4,
                    'key3' => [
                        'field1' => 4,
                        'field2' => true,
                        'field3' => 6,
                        'key5' => [],
                    ],
                ],
            ],
            'key4' => [],
        ];

        $res = Helper::removeNullValues($tb, true);

        $this->assertEquals($expected, $res);
    }

    /**
     * create DateTime object from string date
     */
    public function testCreateDatetimeFromString()
    {
        $strDate1 = '2022-02-15';
        $dt = Helper::createDatetimeFromString($strDate1);
        $this->assertEquals($dt->format('Y-m-d'), $strDate1);

        $strDate11 = '2022-2-14';
        $strDate11Ok = '2022-02-14';
        $dt = Helper::createDatetimeFromString($strDate11);
        $this->assertEquals($dt->format('Y-m-d'), $strDate11Ok);

        $strDate12 = '2022-2-4';
        $strDate12Ok = '2022-02-04';
        $dt = Helper::createDatetimeFromString($strDate12);
        $this->assertEquals($dt->format('Y-m-d'), $strDate12Ok);

        $strDate2 = '2022-02-15 01:12:32';
        $dt = Helper::createDatetimeFromString($strDate2);
        $this->assertEquals($dt->format('Y-m-d H:i:s'), $strDate2);

        $strDate3 = $strDate2 . '.12355';
        $dt = Helper::createDatetimeFromString($strDate3);
        $this->assertEquals($dt->format('Y-m-d H:i:s'), $strDate2);

        $strDate4 = '2022-02-15 01:12';
        $dt = Helper::createDatetimeFromString($strDate4);
        $this->assertEquals($dt->format('Y-m-d H:i'), $strDate4);

        $strDate1 = '15/02/2022';
        $dt = Helper::createDatetimeFromString($strDate1);
        $this->assertEquals($dt->format('d/m/Y'), $strDate1);

        $strDate2 = '15/02/2022 01:12:32';
        $dt = Helper::createDatetimeFromString($strDate2);
        $this->assertEquals($dt->format('d/m/Y H:i:s'), $strDate2);

        $strDate3 = $strDate2 . '.12355';
        $dt = Helper::createDatetimeFromString($strDate3);
        $this->assertEquals($dt->format('d/m/Y H:i:s'), $strDate2);

        // Bad format
        $strDate = '2022-12-32';
        $dt = Helper::createDatetimeFromString($strDate);
        $this->assertNull($dt);
    }

    public function testSlugify()
    {
        $text = 'users.user_id=30 or users.user_id=34';
        $expected = 'users_user_id_30_or_users_user_id_34';
        $slug = Helper::slugify($text);
        $this->assertEquals($expected, $slug);

        $text = 'Stéphane BASSET-CHERCOT';
        $expected = 'stephane_basset-chercot';
        $slug = Helper::slugify($text);
        $this->assertEquals($expected, $slug);


        $text = '.!_Hé toi  __ ! Viens là .';
        $expected = 'he_toi_viens_la';
        $slug = Helper::slugify($text);
        $this->assertEquals($expected, $slug);
    }

    public function testCheckBoolValue()
    {
        $val = true;
        $res = Helper::parseBool($val);
        $this->assertTrue($res);

        $val = false;
        $res = Helper::parseBool($val);
        $this->assertFalse($res);

        $val = "FaLsE";
        $res = Helper::parseBool($val);
        $this->assertFalse($res);

        $val = "test";
        $res = Helper::parseBool($val);
        $this->assertNull($res);
        $val = null;
        $res = Helper::parseBool($val);
        $this->assertNull($res);

        $val = "test";
        $this->expectException(\Wyzen\Php\Exception\HelperException::class);
        Helper::parseBool($val, true);
    }

    /**
     * @throws \Wyzen\Php\Exception\HelperException
     */
    public function testCheckIntValue()
    {

        $val = "test";
        $res = Helper::parseInt($val);
        $this->assertNull($res);

        $val = "12";
        $res = Helper::parseInt($val);
        $this->assertIsInt($res);
        $res = Helper::parseInt($val, true);
        $this->assertIsInt($res);

        $val = 12;
        $res = Helper::parseInt($val);
        $this->assertIsInt($res);
        $res = Helper::parseInt($val, true);
        $this->assertIsInt($res);

        $val = 12.3;
        $res = Helper::parseInt($val);
        $this->assertNull($res);

        $val = "34.5";
        $res = Helper::parseInt($val);
        $this->assertNull($res);

        $this->expectException(\Wyzen\Php\Exception\HelperException::class);
        $val = "34.5";
        Helper::parseInt($val, true);

        $val = "test";
        Helper::parseInt($val, true);
    }

    /**
     * @throws \Wyzen\Php\Exception\HelperException
     */
    public function testCheckFloatValue()
    {

        $val = "test";
        $res = Helper::parseFloat($val);
        $this->assertNull($res);

        $val = "12";
        $res = Helper::parseFloat($val);
        $this->assertIsFloat($res);
        $res = Helper::parseFloat($val, true);
        $this->assertIsFloat($res);

        $val = 12;
        $res = Helper::parseFloat($val);
        $this->assertIsFloat($res);
        $res = Helper::parseFloat($val, true);
        $this->assertIsFloat($res);

        $val = 12.3;
        $res = Helper::parseFloat($val);
        $this->assertIsFloat($res);

        $val = "34.5";
        $res = Helper::parseFloat($val);
        $this->assertIsFloat($res);

        $val = "-00000001.100000";
        $res = Helper::parseFloat($val);
        $this->assertIsFloat($res);

        $this->expectException(\Wyzen\Php\Exception\HelperException::class);
        $val = "34.5t";
        Helper::parseFloat($val, true);

        $val = "test";
        Helper::parseFloat($val, true);
    }

    /**
     * @throws \Wyzen\Php\Exception\HelperException
     */
    public function testCheckArray()
    {

        $val = [];
        $res = Helper::parseArray($val);
        $this->assertIsArray($res);

        $val = ["key" => "val"];
        $res = Helper::parseArray($val);
        $this->assertIsArray($res);

        $val = null;
        $res = Helper::parseArray($val);
        $this->assertNull($res);

        $this->expectException(\Wyzen\Php\Exception\HelperException::class);
        $val = "34.5t";
        Helper::parseArray($val, true);

        $val = null;
        Helper::parseFloat($val, true);
    }

    public function testArrayKeysToTag()
    {
        $tb = [
            'key1' => [
                'field1' => 1,
                'field2' => null,
                'field3' => 2,
            ],
            'key2' => [
                'field1' => false,
                'field2' => null,
                'field3' => 4,
            ],
            'key3' => [
                'field1' => 4,
                'field2' => ["A", "B", "C"],
                'field3' => 6,
                'key33' => [
                    'field31' => 34,
                    'field32' => true,
                    'field33' => 36,
                ],
            ],
            'key4' => [
                'field1' => null,
                'field2' => null,
                'field3' => null,
            ],
        ];

        $expected = [
            "key1/field1" => 1,
            "key1/field2" => null,
            "key1/field3" => 2,
            "key2/field1" => false,
            "key2/field2" => null,
            "key2/field3" => 4,
            "key3/field1" => 4,
            "key3/field2" => ["A", "B", "C"],
            "key3/field3" => 6,
            "key3/key33/field31" => 34,
            "key3/key33/field32" => true,
            "key3/key33/field33" => 36,
            "key4/field1" => null,
            "key4/field2" => null,
            "key4/field3" => null,
        ];

        $res = Helper::arrayKeysToTag($tb, '/');
        
        $this->assertEquals($expected, $res);
    }
}
