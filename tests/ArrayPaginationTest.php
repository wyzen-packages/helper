<?php

use PHPUnit\Framework\TestCase;
use Wyzen\Php\Helper;

class ArrayPaginationTest extends TestCase
{
    public function testArrayPagination_0()
    {
        $data     = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
            'key4' => 'value4',
            'key5' => 'value5',
            'key6' => 'value6',
            'key7' => 'value7',
            'key8' => 'value8',
            'key9' => 'value9',
            'key10' => 'value10',
            'key11' => 'value11',
            'key12' => 'value12',
            'key13' => 'value13',
            'key14' => 'value14',
            'key15' => 'value15',
            'key16' => 'value16',
            'key17' => 'value17',
            'key18' => 'value18',
            'key19' => 'value19',
            'key20' => 'value20',
            'key21' => 'value21',
            'key22' => 'value22',
            'key23' => 'value23',
            'key24' => 'value24',
            'key25' => 'value25',
            'key26' => 'value26',
        ];
        $expected = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
            'key4' => 'value4',
            'key5' => 'value5',
            'key6' => 'value6',
            'key7' => 'value7',
            'key8' => 'value8',
            'key9' => 'value9',
            'key10' => 'value10',
            'key11' => 'value11',
            'key12' => 'value12',
            'key13' => 'value13',
            'key14' => 'value14',
            'key15' => 'value15',
            'key16' => 'value16',
            'key17' => 'value17',
            'key18' => 'value18',
            'key19' => 'value19',
            'key20' => 'value20',
            'key21' => 'value21',
            'key22' => 'value22',
            'key23' => 'value23',
            'key24' => 'value24',
            'key25' => 'value25',
            'key26' => 'value26',
        ];

        $res = Helper::arrayPagination($data, 1, 0);
        $this->assertEquals($expected, $res->results);
    }


    public function testArrayPagination_5_first_page()
    {
        $data     = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
            'key4' => 'value4',
            'key5' => 'value5',
            'key6' => 'value6',
            'key7' => 'value7',
            'key8' => 'value8',
            'key9' => 'value9',
            'key10' => 'value10',
            'key11' => 'value11',
            'key12' => 'value12',
            'key13' => 'value13',
            'key14' => 'value14',
            'key15' => 'value15',
            'key16' => 'value16',
            'key17' => 'value17',
            'key18' => 'value18',
            'key19' => 'value19',
            'key20' => 'value20',
            'key21' => 'value21',
            'key22' => 'value22',
            'key23' => 'value23',
            'key24' => 'value24',
            'key25' => 'value25',
            'key26' => 'value26',
        ];
        $expected = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
            'key4' => 'value4',
            'key5' => 'value5',
        ];

        $res = Helper::arrayPagination($data, 1, 5);
        $this->assertEquals($expected, $res->results);
    }

    public function testArrayPagination_5_second_page()
    {
        $data     = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
            'key4' => 'value4',
            'key5' => 'value5',
            'key6' => 'value6',
            'key7' => 'value7',
            'key8' => 'value8',
            'key9' => 'value9',
            'key10' => 'value10',
            'key11' => 'value11',
            'key12' => 'value12',
            'key13' => 'value13',
            'key14' => 'value14',
            'key15' => 'value15',
            'key16' => 'value16',
            'key17' => 'value17',
            'key18' => 'value18',
            'key19' => 'value19',
            'key20' => 'value20',
            'key21' => 'value21',
            'key22' => 'value22',
            'key23' => 'value23',
            'key24' => 'value24',
            'key25' => 'value25',
            'key26' => 'value26',
        ];
        $expected = [
            'key6' => 'value6',
            'key7' => 'value7',
            'key8' => 'value8',
            'key9' => 'value9',
            'key10' => 'value10',
        ];

        $res = Helper::arrayPagination($data, 2, 5);
        $this->assertEquals($expected, $res->results);
        $this->assertEquals(6, $res->pages);
    }

    public function testArrayPagination_last_page()
    {
        $data     = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
            'key4' => 'value4',
            'key5' => 'value5',
            'key6' => 'value6',
            'key7' => 'value7',
            'key8' => 'value8',
            'key9' => 'value9',
            'key10' => 'value10',
            'key11' => 'value11',
            'key12' => 'value12',
            'key13' => 'value13',
            'key14' => 'value14',
            'key15' => 'value15',
            'key16' => 'value16',
            'key17' => 'value17',
            'key18' => 'value18',
            'key19' => 'value19',
            'key20' => 'value20',
            'key21' => 'value21',
            'key22' => 'value22',
            'key23' => 'value23',
            'key24' => 'value24',
            'key25' => 'value25',
            'key26' => 'value26',
        ];
        $expected = [
            'key26' => 'value26',
        ];

        $res = Helper::arrayPagination($data, 6, 5);
        $this->assertEquals(26, count($data));
        $this->assertEquals($expected, $res->results);
        $this->assertEquals(6, $res->pages);
    }

    public function testArrayPagination_out_pages()
    {
        $data     = [
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3',
            'key4' => 'value4',
            'key5' => 'value5',
            'key6' => 'value6',
            'key7' => 'value7',
            'key8' => 'value8',
            'key9' => 'value9',
            'key10' => 'value10',
            'key11' => 'value11',
            'key12' => 'value12',
            'key13' => 'value13',
            'key14' => 'value14',
            'key15' => 'value15',
            'key16' => 'value16',
            'key17' => 'value17',
            'key18' => 'value18',
            'key19' => 'value19',
            'key20' => 'value20',
            'key21' => 'value21',
            'key22' => 'value22',
            'key23' => 'value23',
            'key24' => 'value24',
            'key25' => 'value25',
            'key26' => 'value26',
        ];
        $expected = [

        ];

        $res = Helper::arrayPagination($data, 7, 5);
        $this->assertEquals($expected, $res->results);
        $this->assertEquals(26, count($data));
        $this->assertEquals(6, $res->pages);
    }
}
