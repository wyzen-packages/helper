<?php

namespace Wyzen\Php\Exception;

use Exception;

/**
 * Class HelperException
 */
class HelperException extends Exception
{
    protected $code    = 501;
    protected $message = "Helper Exception";
}
