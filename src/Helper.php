<?php

namespace Wyzen\Php;

use DateTime;
use Exception;
use stdClass;
use Throwable;
use Wyzen\Php\Exception\HelperException;

/**
 * Class Helper
 */
class Helper
{
    /**
     * Liste des extensions de fichier non désirables
     */
    public const DEFAULT_BLACKLIST_FILE_EXTENSION = ['dll', 'exe', 'sys', 'swf', 'jar', 'lnk', 'gzquar', 'zix', 'js', 'scr', 'scf', 'bat', 'ws', 'vbs', 'bin', 'ocx', 'ozd', 'com', 'aru', 'wmf', 'drv', 'class', 'chm', 'shs', 'pgm', 'dev', 'xlm', 'pif', 'vba', '0_full_0_tgod_signed', 'xnxx', 'boo', 'vbe', 'vxd', 'tps', 'pcx', 'tsa', 'sop', 'hlp', '386', 'bkd', 'vb', 'rhk', 'exe1', 'vbx', 'exe_renamed', 'osa', 'cih', 'lik', 'dyz', 'kcd', 'wsc', 'dyv', 'dlb', 's7p', 'dom', 'php3', 'inf', '.9', 'mjg', 'dxz', 'mfu', 'cla', 'hlw', 'bup', 'rsc_tmp', 'mjz', 'upa', 'bhx', 'mcq', 'dli', 'txs', 'fnr', 'xlv', 'cxq', 'xir', 'xdu', 'wsh', 'bxz', 'ska', 'wlpginstall', 'cfxxe', 'qrn', 'tti', 'vexe', 'dllx', 'fag', 'xtbl', 'smtmp', 'scr', 'ceo', 'tko', 'uzy', 'oar', 'bll', 'spam', 'plc', 'ssy', 'dbd', 'smm', 'ce0', 'zvz', 'blf', 'cc', 'ctbl', 'iws', 'vzr', 'nls', 'hsq', 'lkh', 'rna', 'aepl', 'hts', 'let', 'aut', 'buk', 'delf', 'atm', 'ezt', 'fuj', 'fjl', 'bmw', 'dx', 'cyw', 'iva', 'pid', 'bps', 'capxml', 'bqf', 'pr', 'qit', 'lpaq5', 'xnt', 'lok', 'doc', 'xls', 'ppt', 'docm', 'dotm', 'xlsm', 'xltm', 'xlam', 'pptm', 'potm', 'ppam', 'ppsm', 'sldm', 'msh', 'msh1', 'msh2', 'mshxml', 'msh1xml', 'msh2xml',];
    private static array $BLACKLIST_FILE_EXTENSION = self::DEFAULT_BLACKLIST_FILE_EXTENSION;


    /***
     *    ███████╗██╗██╗     ███████╗
     *    ██╔════╝██║██║     ██╔════╝
     *    █████╗  ██║██║     █████╗
     *    ██╔══╝  ██║██║     ██╔══╝
     *    ██║     ██║███████╗███████╗
     *    ╚═╝     ╚═╝╚══════╝╚══════╝
     *
     */


    /**
     * Set blacklist extensions
     *
     * @param array $ext
     *
     * @return array
     */
    public static function setBlacklistExtensionFile(array $ext): array
    {
        self::$BLACKLIST_FILE_EXTENSION = $ext;
        return $ext;
    }

    /**
     * Get blacklist extensions
     *
     * @return array
     */
    public static function getBlacklistExtensionFile(): array
    {
        return self::$BLACKLIST_FILE_EXTENSION;
    }



    /***
     *     █████╗ ██████╗ ██████╗  █████╗ ██╗   ██╗
     *    ██╔══██╗██╔══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝
     *    ███████║██████╔╝██████╔╝███████║ ╚████╔╝
     *    ██╔══██║██╔══██╗██╔══██╗██╔══██║  ╚██╔╝
     *    ██║  ██║██║  ██║██║  ██║██║  ██║   ██║
     *    ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝
     *
     */

    /**
     * Return value from array assoc
     *
     * @param array|null $a
     * @param string $key
     * @param mixed|null $defaultValueOrException
     * @param array|null $classMethodValidator
     * @return mixed
     * @return mixed
     * @throws Exception
     * @example $result = Helper::getValueEx($data, 'myKey', 'defaultValue');
     *
     * @example $result = Helper::getValueEx($data, 'myKey');
     */
    public static function getValueEx(?array $a, string $key, $defaultValueOrException = null, ?array $classMethodValidator = null)
    {
        $result = null;

        try {
            if (!\is_array($a)) {
                throw new \InvalidArgumentException(__FUNCTION__ . ' $a is not an array');
            }

            if (array_key_exists($key, $a)) {
                return $a[$key];
            }

            throw new \InvalidArgumentException(__FUNCTION__ . " Key '$key' not exists");
        } catch (\Exception $ex) {
            if (\is_null($defaultValueOrException)) {
                return null;
            }

            if (\is_string($defaultValueOrException) && \class_exists($defaultValueOrException) && array_key_exists(Throwable::class, \class_implements($defaultValueOrException))) {

                /**
                 * @var Exception $defaultValueOrException
                 */
                throw new $defaultValueOrException($ex->getMessage(), $ex->getCode());
            }

            return $defaultValueOrException;
        }
    }

    /**
     * Get data from research in array
     *
     * @example Helper::findInArrayByKeys($data, 'key1', 'subKey1', 'subsubkey1'); // return $data['key1']['subkey1']['subkey1']
     * @example Helper::findInArrayByKeys($data); // Return all
     * @example Helper::findInArrayByKeys($data, 'key1', 'keyNotExists'); // return null
     *
     * @param array $data array of data
     * @param array ...$keys List of keys
     *
     * @return mixed
     */
    public static function findInArrayByKeys(array &$data, ...$keys)
    {
        if (\count($keys) === 0) {
            return $data;
        }

        $result = $data;
        /** @var string $key */
        foreach ($keys as $key) {
            if (!\array_key_exists($key, $result)) {
                return null;
            }
            $result = $result[$key];
        }
        return $result;
    }

    /**
     * Compare 2 tableaux de valeurs ou 2 valeurs
     *
     * @param mixed|array $arr1 La variable sera transformée en tableau
     * @param mixed|array $arr2 La variable sera transformée en tableau
     * @param string $operateur default(IN), NOTIN, EQ
     *
     * @return boolean
     */
    public static function compareValues($arr1 = [], $arr2 = [], $operateur = 'IN'): bool
    {
        if (!is_array($arr1)) {
            $arr1 = [$arr1];
        }
        if (!is_array($arr2)) {
            $arr2 = [$arr2];
        }

        switch (strtoupper($operateur)) {
            case 'IN':
                $result = (count(array_intersect($arr1, $arr2)) !== 0);
                break;

            case 'NOTIN':
                $result = (count(array_diff($arr1, $arr2)) !== 0);
                break;

            case 'EQ':
                $result = (count(array_intersect($arr1, $arr2)) == count($arr2));
                break;

            default:
                $result = false;
                break;
        }

        return $result;
    }

    /**
     * Return new array indexed by $reorderByKeyname
     *
     * @param array $array array assoc or array objects
     * @param string $reorderByKeyname
     * @param bool $append add new Row if multiple same key
     *
     * @return array
     */
    public static function indexedByKey(array &$array, $reorderByKeyname, $append = false): array
    {
        $result = [];

        foreach ($array as $row) {
            $valOfKey = (\is_object($row)) ? $row->$reorderByKeyname : $row[$reorderByKeyname];
            if ($append === false) {
                $result[$valOfKey] = $row;
            } else {
                $result[$valOfKey][] = $row;
            }
        }

        return $result;
    }

    /**
     * Retourne la valeur dans un tableau
     * tag key1.key2 => $data['key1']['key2']="ma valeur";
     *
     * @param string $tag key1.key2.key_n
     * @param array $data array assoc
     *
     * @return mixed
     */
    public static function getValueFromTag(string $tag, array &$data)
    {
        $akeyTag = explode(".", $tag);
        return self::findInArrayByKeys($data, ...$akeyTag);
    }

    /**
     * Retourne la valeur dans un tableau
     * tag key1.key2 => $data['key1']['key2']="ma valeur";
     *
     * @param string $tag key1.key2.key_n
     * @param array $data array assoc
     *
     * @return mixed
     */
    public static function findInArrayByTag(array &$data, string $tag)
    {
        $akeyTag = explode(".", $tag);
        return self::findInArrayByKeys($data, ...$akeyTag);
    }

    /***
     *    ██╗   ██╗ █████╗ ██╗     ██╗██████╗  █████╗ ████████╗██╗ ██████╗ ███╗   ██╗
     *    ██║   ██║██╔══██╗██║     ██║██╔══██╗██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║
     *    ██║   ██║███████║██║     ██║██║  ██║███████║   ██║   ██║██║   ██║██╔██╗ ██║
     *    ╚██╗ ██╔╝██╔══██║██║     ██║██║  ██║██╔══██║   ██║   ██║██║   ██║██║╚██╗██║
     *     ╚████╔╝ ██║  ██║███████╗██║██████╔╝██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║
     *      ╚═══╝  ╚═╝  ╚═╝╚══════╝╚═╝╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝
     *
     */

    /**
     * Check if date is 'Y-m-d H:i:s'
     *
     * @param string $date
     * @return boolean
     */
    public static function isDateWithValidFormat(string $date): bool
    {
        return (DateTime::createFromFormat('Y-m-d H:i:s', $date) !== false);
    }



    /***
     *     ██████╗ ████████╗██╗  ██╗███████╗██████╗
     *    ██╔═══██╗╚══██╔══╝██║  ██║██╔════╝██╔══██╗
     *    ██║   ██║   ██║   ███████║█████╗  ██████╔╝
     *    ██║   ██║   ██║   ██╔══██║██╔══╝  ██╔══██╗
     *    ╚██████╔╝   ██║   ██║  ██║███████╗██║  ██║
     *     ╚═════╝    ╚═╝   ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝
     *
     */


    /**
     * Retourne une chaine en camelCase avec lcfirst|ucfirst
     *
     * @param string $str
     * @param string $firstChar lcfirst|ucfirst default(lcfirst)
     *
     * @return string
     */
    public static function camelCase($str, $firstChar = 'lcfirst')
    {
        $str = str_replace(['-', '_', '.'], ' ', $str);
        $str = mb_convert_case($str, MB_CASE_TITLE);
        $str = str_replace(' ', '', $str); //ucwords('ghjkiol|ghjklo', "|");

        if (!function_exists($firstChar)) {
            $firstChar = 'lcfirst';
        }
        $str = call_user_func($firstChar, $str);

        return $str;
    }

    /**
     * Return random str whith characters in $keyspace
     *
     * @param int $length
     * @param string $keyspace
     *
     * @return string
     * @throws Exception
     */
    public static function randomStr(
        int    $length,
        string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ): string
    {
        $str = '';
        $max = \mb_strlen($keyspace, '8bit') - 1;
        if ($max < 1) {
            throw new \Exception('$keyspace must be at least two characters long');
        }
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[\random_int(0, $max)];
        }
        return $str;
    }

    /**
     * return a password auto generated
     *
     * @param integer $min_length Minimum total length default 8
     * * @param integer $max_length Maximum total length default 8
     * @param array $options variables configuration
     *
     * @return string
     * @throws Exception
     */
    public static function passwordGenerate(int $min_length = 8, int $max_length = 8, array $options = []): string
    {
        $min_letters = Helper::getValueEx($options, 'min_letters', 4);
        $min_numbers = Helper::getValueEx($options, 'min_numbers', 1);
        $min_specials = Helper::getValueEx($options, 'min_specials', 1);

        $numeric = Helper::getValueEx($options, 'numeric', '0123456789');
        $alphabet = Helper::getValueEx($options, 'alphabet', 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz');
        $special = Helper::getValueEx($options, 'special', '!@#$%&*()_-=+;:,.?');

        $password = '';
        $numbers = 0;
        $letters = 0;
        $specials = 0;
        $length = 0;

        while (
            $length <= $min_length
            || $numbers <= $min_numbers
            || $specials <= $min_specials
            || $letters <= $min_letters
        ) {
            $length += 1;
            $type = rand(1, 3);

            if ($type == 1) {
                $password .= $numeric[rand(0, \strlen($numeric) - 1)]; //Numbers and special characters
                $numbers += 1;
            } elseif ($type == 2) {
                $password .= $alphabet[rand(0, \strlen($alphabet) - 1)];
                $letters += 1;
            } elseif ($type == 3) {
                $password .= $special[rand(0, \strlen($special) - 1)];
                $specials += 1;
            }

            if ($length == $max_length) {
                break;
            }
        }
        return $password;
    }


    /**
     * Converti une chaine ISO-8859-15 en UTF8
     *
     * @param string $str
     *
     * @return string
     */
    public static function isoToUtf8($str = '')
    {
        return iconv('ISO-8859-15', 'UTF-8//TRANSLIT', $str);
    }

    /**
     * Converti une chaine UTF-8 en ISO-8859-15
     *
     * @param string $str
     *
     * @return string
     */
    public static function utf8ToIso($str = ''): string
    {
        return iconv('UTF-8', 'ISO-8859-15//TRANSLIT', $str);
    }

    /**
     * Normalise une chaîne de caractères pour un nom de fichier
     *
     * @param string|null $str string to normalize
     *
     * @return string
     */
    public static function normalizeTitleDocument(?string $str): string
    {
        return str_replace(' ', '_', $str);
    }


    /**
     * Get the class "basename" of the given object / class.
     *
     * @param string|object $class
     * @return string
     */
    public static function classBasename($class): string
    {
        $class = is_object($class) ? get_class($class) : $class;

        return basename(str_replace('\\', '/', $class));
    }

    /**
     * Supprime les valeurs NULL des sous clés d'un tableau
     *
     * @param array $tb
     * @param bool $recursive recherche en profondeur
     *
     * @return array
     */
    public static function removeNullValues(array &$tb = [], bool $recursive = false): array
    {
        $res = [];
        foreach ($tb as $domain => $data) {
            if (\is_array($data)) {
                if ($recursive) {
                    $res[$domain] = self::removeNullValues($data, $recursive);
                } else {
                    $res[$domain] = \array_filter($data, function ($val) {
                        return !\is_null($val);
                    });
                }
            } else {
                if (!\is_null($data)) {
                    $res[$domain] = $data;
                }
            }
        }
        return $res;
    }

    /**
     * create DateTime object from string date
     * @param String|null $strDate 'Y-M-D', 'Y-M-D H:i:s', 'Y-M-D H:i:s.1234', 'D/M/Y', 'D/M/Y H:i:s'
     *
     * @return \DateTime|null return null if error
     * @throws Exception
     * @example 2022-1-4, 2022-01-04, 4/1/2022, 04/01/2022
     */
    public static function createDatetimeFromString(?string $strDate, ?string $format = null): ?DateTime
    {
        $REGEX_SQL = "/(?<fulldate>(?<Y>\d{4})(-)*(?<M>\d{1,2})(-)*(?<D>\d{1,2}))(( ){1}(?<fulltime>(?<h>\d{2}):(?<m>\d{2})(:(?<s>\d{2})){0,1}(:(\d{2})){0,1})){0,1}/";
        $REGEX_STR = "/(?<fulldate>(?<D>\d{1,2})(\/)*(?<M>\d{1,2})(\/)*(?<Y>\d{4}))(( ){1}(?<fulltime>(?<h>\d{2}):(?<m>\d{2})(:(?<s>\d{2})){0,1}(:(\d{2})){0,1})){0,1}/";


        if (\is_null($strDate)) {
            return null;
        }

        if (!\is_null($format)) {
            $ret = DateTime::createFromFormat($format, $strDate);
            return ($ret === false) ? null : $ret;
        }

        if (preg_match($REGEX_SQL, $strDate, $dateSplitted) || preg_match($REGEX_STR, $strDate, $dateSplitted)) {
            if (\checkdate(intval($dateSplitted['M']), intval($dateSplitted['D']), intval($dateSplitted['Y']))) {
                $M = Helper::getValueEx($dateSplitted, 'M', '00');
                $D = Helper::getValueEx($dateSplitted, 'D', '00');
                $Y = Helper::getValueEx($dateSplitted, 'Y', '00');
                $h = Helper::getValueEx($dateSplitted, 'h', '00');
                $m = Helper::getValueEx($dateSplitted, 'm', '00');
                $s = Helper::getValueEx($dateSplitted, 's', '00');

                $ret = DateTime::createFromFormat('Y-m-d H:i:s', "$Y-$M-$D $h:$m:$s");
                return ($ret === false) ? null : $ret;
            }
        }
        return null;
    }

    /**
     * manage pagination from array
     *
     * @param array $data
     * @param integer $page number of page (1...n)
     * @param integer $limit if 0 return all data
     *
     * @return stdClass(limit,total,page,pages,results)
     */
    public static function arrayPagination(array &$data, int $page = 1, $limit = 20): stdClass
    {
        $total = \count($data);
        if ($page < 0 || $limit < 0) {
            return (object)[
                'limit' => $limit,
                'total' => 0,
                'page' => $page,
                'pages' => 0,
                'results' => []
            ];
        }

        /**
         * If limit = 0 returns original data
         */
        if ($limit === 0) {
            return (object)[
                'limit' => $limit,
                'total' => $total,
                'page' => $page,
                'pages' => 1,
                'results' => $data
            ];
        }

        $length = count($data);
        $pages = ceil($length / $limit);

        $offset = (($page - 1) * $limit);

        $results = array_slice($data, $offset, $limit, true);

        $array_result = [
            'limit' => $limit,
            'total' => $total,
            'page' => $page,
            'pages' => $pages,
            'results' => $results
        ];

        return (object)$array_result;
    }

    /**
     * return the slug of string
     *
     * @param string $text
     * @param string $divider
     *
     * @return string
     */
    public static function slugify(string $text, string $divider = '_'): string
    {
        //         $rules = <<<RULES
        // :: Any-Latin;
        // :: NFD;
        // :: [:Nonspacing Mark:] Remove;
        // :: NFC;
        // :: [^-[:^Punctuation:]] Remove;
        // :: Lower();
        // [:^L:] { [-] > ;
        // [-] } [:^L:] > ;
        // [-[:Separator:]]+ > '$divider';
        // RULES;
        //         return \Transliterator::createFromRules($rules)->transliterate($text);

        if (empty($text)) {
            return 'n' . $divider . 'a';
        }

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        $text = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', $divider, $text)));
        // Supprime le divider de debut et de fin
        $text = preg_replace("~^$divider~", '', $text);
        $text = preg_replace("~$divider\$~", '', $text);

        return $text;
    }

    /**
     * Return bool|null value
     * @param null|string|bool $val : Accept only 1,0,true,false,"true","false"
     * @param bool $strict : only bool value, not null
     * @return bool|null
     * @throws HelperException
     */
    public static function parseBool($val, bool $strict = false): ?bool
    {
        if (is_bool($val)) {
            return boolval($val);
        }

        if (strtolower($val) === "true") {
            return true;
        }

        if (strtolower($val) === "false") {
            return false;
        }

        if ($strict) {
            throw new HelperException("Required only bool value");
        }
        // Return null on other $val
        return null;
    }

    /**
     * Return int|null value
     * @param null|string|int $val : Accept only 12, "12", not 12.2
     * @param bool $strict : only bool value, not null
     * @return int|null
     * @throws HelperException
     */
    public static function parseInt($val, bool $strict = false): ?int
    {
        $isNumeric = is_numeric($val);

        if ($strict && !$isNumeric) {
            throw new HelperException("Required only int value");
        }
        if (!$isNumeric) {
            return null;
        }

        $intval = intval($val);
        if ($intval != $val) {
            return null;
        }

        return $intval;
    }

    /**
     * Return int|null value
     * @param null|string|float $val : Accept only 12, "12", "12.2"
     * @param bool $strict : only bool value, not null
     * @return float|null
     * @throws HelperException
     */
    public static function parseFloat($val, bool $strict = false): ?float
    {
        $isNumeric = is_numeric($val);

        if ($strict && !$isNumeric) {
            throw new HelperException("Required only float value");
        }
        if (!$isNumeric) {
            return null;
        }

        $floatval = floatval($val);
        if ($floatval != $val) {
            return null;
        }

        return $floatval;
    }

    /**
     * Return array|null value
     * @param $val
     * @param bool $strict
     * @return array|null
     * @throws HelperException
     */
    public static function parseArray($val, bool $strict = false): ?array
    {
        if (is_array($val)) {
            return $val;
        }

        if ($strict) {
            throw new HelperException("Required only array value");
        }
        return null;
    }

    /**
     * Converti un tableau avec ses sous-tableaux en tableau de tags
     * @param array $array
     * @param string $separator
     * @param string|null $prefix
     * @return array
     */
    public static function arrayKeysToTag(array $array = [], string $separator = '.', ?string $prefix = null): array
    {
        $keysWithValues = [];

        foreach ($array as $key => $value) {
            $newKey = is_null($prefix) ? $key : $prefix . $separator . $key;

            if (is_array($value)) {
                // Si le tableau est indexé numériquement (non associatif)
                if (array_keys($value) === range(0, count($value) - 1)) {
                    $keysWithValues[$newKey] = $value; // Regroupe les valeurs dans un tableau
                } else {
                    $keysWithValues = array_merge($keysWithValues, self::arrayKeysToTag($value, $separator, $newKey));
                }
            } else {
                $keysWithValues[$newKey] = $value;
            }
        }

        return $keysWithValues;
    }
}
