#!/bin/sh
BASEDIR="$(cd "$(dirname "$0")/.." && pwd)"
ACTION=${*:-""}
OPTIONS=""
PUID=$(id -u)
GUID=$(id -g)

cd $BASEDIR &&
    docker run -it ${OPTIONS} --user="$PUID:$GUID" -v $(pwd):/application wyzenrepo/php-nginx:7.4 composer $ACTION
